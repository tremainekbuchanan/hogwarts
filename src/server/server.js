var express = require("express");
var bodyParser = require("body-parser");
var cors = require("cors");
var mongoose = require("mongoose");
var User = require("./models/user.js");
var Student = require("./models/student.js");
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.get('/', function(req, res){
	res.json({message: 'Home'});
});

var db = mongoose.connection;

db.on('error', console.error);
db.once('open', function(){
	console.log('Database opened');
});

mongoose.connect('mongodb://localhost/hsrm');

app.post('/login',function(req, res){
  var username=req.body.user;
  var password=req.body.password;
  var message;
  console.log("User name = "+username+", password is "+password);
  User.findOne(username, function(err,user){
		if(err) res.send(err);
		if(user.password === password){
			res.json({message: "Success"});
		}
	});
  //res.json(message);
});

app.post('/students', function(req, res){
	var student = {
		 name : req.body.name,
		 address : req.body.address,
		 type : req.body.type,
		 house : req.body.house,
		 quid_player	: req.body.player,
		 tests		: [],
		 final_grade : 0,   
		 deleted : false 
	};	
	console.log(student);
	res.json({message: "created"});
});

app.listen(8080);
console.log('Server started');