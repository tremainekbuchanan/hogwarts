var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UserSchema = new Schema({
	name    	: String,
	uname		: String,
	password	: String,
	rights		: [String]
});

module.exports = mongoose.model('users', UserSchema);