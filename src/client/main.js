angular.module('hsrm',[
	'DashboardCtrl',
	'LoginCtrl',
	'StudentCtrl',
	'CreateStudentCtrl',
	'UserService',
	'StudentService',
	'ngRoute'
]).config(['$routeProvider', function($routeProvider, $locationProvider){
	$routeProvider
	.when("/",{ 
		templateUrl: "partials/login.html",
		controller: "LoginController",
	
	}).when("/dashboard",{
		templateUrl : "partials/dashboard.html",
		controller : "DashboardController"
	
	}).when("/reports",{
		templateUrl : "partials/report.html",
		controller : "ReportCtrl"
	
	}).when("/students",{
		templateUrl : "partials/students.html",
		controller : "StudentController"

	}).when("/students/create",{
		templateUrl : "partials/forms/student.html",
		controller : "CreateStudentController"

	}).when("/students/profile",{
		templateUrl : "partials/profile.html",
		controller : "ProfileCtrl"
	
	}).when("/students/profile/tests",{
		templateUrl : "partials/forms/test.html",
		controller : "CreateTestCtrl"
	
	}).otherwise({redirectTo: "/"});

	// $locationProvider.html5Mode(true);
}]);