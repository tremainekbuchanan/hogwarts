angular.module('CreateStudentCtrl',[])
.controller('CreateStudentController', function($scope, $location, Student){
	$scope.createStudent = function(){
		var student = {
			name: $scope.name,
			address: $scope.address,
			type: $scope.type ,
			house:$scope.house,
			player: $scope.player 
	}
		Student.create(student).success(function(response){
			console.log(response);
		});
		//console.log("Saved");
		//$location.path('/students/profile');
	}
});