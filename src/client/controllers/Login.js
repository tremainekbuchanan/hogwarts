angular.module('LoginCtrl',[])
.controller('LoginController',function($scope, $location, User){
	$scope.message = [];
	$scope.username = "";
	$scope.password = "";
	$scope.submit = function(){
		if(!$scope.username && !$scope.password){
			console.log('empty');
		}else{
				var userdata = {user: $scope.username, password: $scope.password};
				User.login(userdata).success(function(response){
					if(response.message === "Success")
						$location.path("/dashboard");
					else{
						 console.log("Error");
						$location.path("/");
					}			
						
				});
			}
	}//end of function
});//end of controller