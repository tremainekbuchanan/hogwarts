angular.module('hsrm.controllers',[])
.controller('LoginCtrl',function($scope, $location, Users){
	$scope.message = [];
	$scope.username = "";
	$scope.password = "";
	$scope.submit = function(){
		if(!$scope.username && !$scope.password){
			console.log('empty');
		}else{
				var userdata = {user: $scope.username, password: $scope.password};
				Users.login(userdata).success(function(response){
					if(response.message === "Success")
						$location.path("/dashboard");
					else{
						console.log("Error");
						$location.path("/");
					}			
						
				});
			}
	}	
}).controller('Dashboard', function($scope){
	console.log("In dashboard");

});